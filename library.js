'use strict';

var async =  module.parent.require('async');
var translator = require.main.require('./public/src/modules/translator');

var Parser = require('rss-parser');
var dateformat = require('dateformat');

var app;

var Widget = module.exports;

Widget.init = function(params, callback) {
	app = params.app;

	callback();
};

Widget.renderParseRssWidget = function(widget, callback) {
	var address = widget.data.address;
	var maxPosts = widget.data.countfeed == parseInt(widget.data.countfeed, 10) && widget.data.countfeed >= 0 ? widget.data.countfeed : 5;
	var dateformatstring = widget.data.dateformatstring || 'ddd, dd mmm yyyy HH:MM';

	var parser = new Parser();

	async.waterfall([
		async.asyncify(function() {
			try {
				return parser.parseURL(address);
			} catch(err) {
				return null;
			}
		}),
		function (rawfeeds, next) {
			getFeedItems(rawfeeds, maxPosts, dateformatstring, next);
		},
		function (feeds, next) {
			app.render('widget/parserss', {
				feeds: feeds
			}, next);
		},
		function (html, next) {
			widget.html = html;
			next(null, widget);
		}
	], callback);
};

function getFeedItems(feeds, maxPosts, dateformatstring, callback) {
	feeds = feeds || {};
	maxPosts = Array.isArray(feeds.items) ? Math.min(maxPosts, feeds.items.length) : 0;
	var feedArray = [];

	translator.translate('[[parserss:dateformat]]', function(translated) {
		var dateformatI18n = JSON.parse(translated);
		dateformat.i18n = dateformatI18n;
	});

	for(var i=0; i<maxPosts; i++) {
		if(feeds.items[i].pubDate) {
			var then = new Date(feeds.items[i].pubDate);
			try {
				feeds.items[i].pubDate = dateformat(then, dateformatstring);
			} catch(err) {
				feeds.items[i].pubDate = dateformat(then, 'ddd, dd mmm yyyy HH:MM');
			}
		}
		feedArray.push(feeds.items[i]);
	}

	callback(null, feedArray);
}

Widget.defineWidgets = function(widgets, callback) {
	async.waterfall([
		function(next) {
			app.render('admin/parserss', {}, function(err, html) {
				widgets.push({
					widget: "parserss",
					name: "Parse-RSS",
					description: "List of RSS-Feed.",
					content: html
				});
				next(err, widgets);
			});
		}
	], callback);
};